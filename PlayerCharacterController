using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CharacterController), typeof(PlayerInputHandler), typeof(HealthPoints))]
public class PlayerCharacterController : MonoBehaviour
{
    public UnityEvent OnStepHasBeenTaken;
    public UnityEvent OnJumpHasBeenMade;
    public UnityEvent OnLanding;
    public UnityEvent OnHeavyLanding;

    public UnityAction<bool> OnStanceChanged;

    public bool IsGrounded { get; private set; }
    public bool HasJumpedThisFrame { get; private set; }
    public bool IsCrouching { get; private set; }
    public bool IsAiming = false;

    public float RotationMultiplier
    {
        get
        {
            if (IsAiming)
            {
                return aimingRotationMultiplier;
            }

            return 1f;
        }
    }

    [SerializeField] private Transform playerCamera;
    [SerializeField] private float gravityDownForce = 20f;
    [SerializeField] private float groundCheckDistance = 0.05f;
    [SerializeField] private float maxSpeedOnGround = 5f;
    [SerializeField] private float movementSharpnessOnGround = 15;
    [SerializeField] private float maxSpeedCrouchedRatio = 0.5f;
    [SerializeField] private float maxSpeedInAir = 10f;
    [SerializeField] private float accelerationSpeedInAir = 25f;
    [SerializeField] private float sprintSpeedModifier = 1.5f;
    [SerializeField] private float killHeight = -50f;
    [SerializeField] private float rotationSpeed = 200f;
    [SerializeField] private float aimingRotationMultiplier = 0.4f;
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private float cameraHeightRatio = 0.8f;
    [SerializeField] private float capsuleHeightStanding = 1.8f;
    [SerializeField] private float capsuleHeightCrouching = 0.9f;
    [SerializeField] private float crouchingSharpness = 10f;
    [SerializeField] private float footstepSfxFrequency = 0.1f;
    [SerializeField] private float minSpeedForFallDamage = 10f;
    [SerializeField] private float maxSpeedForFallDamage = 30f;
    [SerializeField] private float fallDamageAtMinSpeed = 10f;
    [SerializeField] private float fallDamageAtMaxSpeed = 50f;
    [SerializeField] private bool recievesFallDamage = true;
    [SerializeField] private LayerMask GroundCheckLayers = -1;

    private float lastTimeJumped = 0f;
    private float cameraVerticalAngle = 0f;
    private float footstepDistanceCounter;
    private float targetCharacterHeight;

    private Vector3 CharacterVelocity { get; set; }
    private PlayerInputHandler inputH;
    private CharacterController control;
    private Vector3 groundNormal;
    private Vector3 latestImpactSpeed;
    private HealthPoints hp;

    private const float k_JumpGroundingPreventionTime = 0.2f;
    private const float k_GroundCheckDistanceInAir = 0.07f;

    void Awake()
    {
        control = GetComponent<CharacterController>();
        inputH = GetComponent<PlayerInputHandler>();
        hp = GetComponent<HealthPoints>();

        control.enableOverlapRecovery = true;

        SetCrouchingState(false, true);
        UpdateCharacterHeight(true);
    }

    void LateUpdate()
    {
        if (hp.IsAlive && transform.position.y < killHeight)
        {
            hp.TakeDamage(100f);
        }

        HasJumpedThisFrame = false;

        bool wasGrounded = IsGrounded;
        GroundCheck();

        if (IsGrounded && !wasGrounded)
        {
            float fallSpeed = -Mathf.Min(CharacterVelocity.y, latestImpactSpeed.y);
            float fallSpeedRatio = (fallSpeed - minSpeedForFallDamage) / (maxSpeedForFallDamage - minSpeedForFallDamage);

            if (recievesFallDamage && fallSpeedRatio > 0f)
            {
                float dmgFromFall = Mathf.Lerp(fallDamageAtMinSpeed, fallDamageAtMaxSpeed, fallSpeedRatio);
                hp.TakeDamage(dmgFromFall);

                CharacterVelocity = Vector3.zero;

                OnHeavyLanding.Invoke();
            }
            else
            {
                OnLanding.Invoke();
            }
        }

        if (inputH.GetCrouchInputDown())
        {
            SetCrouchingState(!IsCrouching, false);
        }

        UpdateCharacterHeight(false);

        HandleCharacterMovement();
    }

    void GroundCheck()
    {
        float chosenGroundCheckDistance = IsGrounded ? (control.skinWidth + groundCheckDistance) : k_GroundCheckDistanceInAir;

        IsGrounded = false;
        groundNormal = Vector3.up;

        if (Time.time >= lastTimeJumped + k_JumpGroundingPreventionTime)
        {
            if (Physics.SphereCast(GetCapsuleBottomHemisphere(), control.radius, Vector3.down, out RaycastHit hit, chosenGroundCheckDistance, GroundCheckLayers, QueryTriggerInteraction.Ignore))
            {
                groundNormal = hit.normal;

                if (Vector3.Dot(hit.normal, transform.up) > 0f && IsNormalUnderSlopeLimit(groundNormal))
                {
                    IsGrounded = true;

                    if (hit.distance > control.skinWidth)
                    {
                        control.Move(Vector3.down * hit.distance);
                    }
                }
            }
        }
    }

    void HandleCharacterMovement()
    {
        transform.Rotate(new Vector3(0f, (inputH.GetLookInputsHorizontal() * rotationSpeed * RotationMultiplier), 0f), Space.Self);

        cameraVerticalAngle += inputH.GetLookInputsVertical() * rotationSpeed * RotationMultiplier;

        cameraVerticalAngle = Mathf.Clamp(cameraVerticalAngle, -89f, 89f);

        playerCamera.transform.localEulerAngles = new Vector3(cameraVerticalAngle, 0, 0);


        bool isSprinting = inputH.GetSprintInputHeld();
        {

            Vector3 worldspaceMoveInput = transform.TransformVector(inputH.GetMoveInput());

            if (IsGrounded)
            {
                if (isSprinting)
                {
                    isSprinting = SetCrouchingState(false, false);
                }

                float speedModifier = isSprinting ? sprintSpeedModifier : 1f;

                Vector3 targetVelocity = worldspaceMoveInput * maxSpeedOnGround * speedModifier;

                if (IsCrouching)
                {
                    targetVelocity *= maxSpeedCrouchedRatio;
                }

                targetVelocity = Vector3.ProjectOnPlane(targetVelocity, groundNormal).normalized * targetVelocity.magnitude;

                CharacterVelocity = Vector3.Lerp(CharacterVelocity, targetVelocity,movementSharpnessOnGround * Time.deltaTime);

                if (IsGrounded && inputH.GetJumpInputDown())
                {
                    if (SetCrouchingState(false, false))
                    {
                        CharacterVelocity = new Vector3(CharacterVelocity.x, 0f, CharacterVelocity.z);

                        CharacterVelocity += Vector3.up * jumpForce;

                        OnJumpHasBeenMade.Invoke();

                        lastTimeJumped = Time.time;
                        HasJumpedThisFrame = true;

                        IsGrounded = false;
                        groundNormal = Vector3.up;
                    }
                }

                float chosenFootstepSfxFrequency = (CharacterVelocity.magnitude/2) * footstepSfxFrequency;

                if (footstepDistanceCounter >= 1f / chosenFootstepSfxFrequency)
                {
                    footstepDistanceCounter = 0f;

                    OnStepHasBeenTaken.Invoke();
                }

                footstepDistanceCounter += CharacterVelocity.magnitude * Time.deltaTime;
            }
            else
            {
                CharacterVelocity += worldspaceMoveInput * accelerationSpeedInAir * Time.deltaTime;

                float verticalVelocity = CharacterVelocity.y;
                Vector3 horizontalVelocity = Vector3.ProjectOnPlane(CharacterVelocity, Vector3.up);
                horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, maxSpeedInAir);
                CharacterVelocity = horizontalVelocity + (Vector3.up * verticalVelocity);

                CharacterVelocity += Vector3.down * gravityDownForce * Time.deltaTime;
            }
        }

        Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
        Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(control.height);
        control.Move(CharacterVelocity * Time.deltaTime);

        latestImpactSpeed = Vector3.zero;

        if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, control.radius, CharacterVelocity.normalized, out RaycastHit hit, CharacterVelocity.magnitude * Time.deltaTime, -1, QueryTriggerInteraction.Ignore))
        {
            latestImpactSpeed = CharacterVelocity;

            CharacterVelocity = Vector3.ProjectOnPlane(CharacterVelocity, hit.normal);
        }
    }

    bool IsNormalUnderSlopeLimit(Vector3 normal)
    {
        return Vector3.Angle(transform.up, normal) <= control.slopeLimit;
    }
 
    Vector3 GetCapsuleBottomHemisphere()
    {
        return transform.position + (transform.up * control.radius);
    }

    Vector3 GetCapsuleTopHemisphere(float atHeight)
    {
        return transform.position + (transform.up * (atHeight - control.radius));
    }

    void UpdateCharacterHeight(bool force)
    {

        if (force)
        {
            control.height = targetCharacterHeight;
            control.center = Vector3.up * control.height * 0.5f;
            playerCamera.transform.localPosition = Vector3.up * targetCharacterHeight * cameraHeightRatio;
        }
        else if (control.height != targetCharacterHeight)
        {
            control.height = Mathf.Lerp(control.height, targetCharacterHeight, crouchingSharpness * Time.deltaTime);
            control.center = Vector3.up * control.height * 0.5f;

            playerCamera.transform.localPosition = Vector3.Lerp(playerCamera.transform.localPosition, Vector3.up * targetCharacterHeight * cameraHeightRatio, crouchingSharpness * Time.deltaTime);
        }
    }

    bool SetCrouchingState(bool crouched, bool ignoreObstructions)
    {
        if (crouched)
        {
            targetCharacterHeight = capsuleHeightCrouching;
        }
        else
        {
            if (!ignoreObstructions)
            {
                Collider[] standingOverlaps = Physics.OverlapCapsule(GetCapsuleBottomHemisphere(),GetCapsuleTopHemisphere(capsuleHeightStanding), control.radius,-1,QueryTriggerInteraction.Ignore);

                foreach (Collider c in standingOverlaps)
                {
                    if (c != control)
                    {
                        return false;
                    }
                }
            }

            targetCharacterHeight = capsuleHeightStanding;
        }

        if (OnStanceChanged != null)
        {
            OnStanceChanged.Invoke(crouched);
        }

        IsCrouching = crouched;
        return true;
    }
}
